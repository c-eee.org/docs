module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Learn More',
      items: [
        'learn-more',
        ],
    },
    {
      type: 'category',
      label: 'Zimbra Recipes',
      items: [
        'zimbra/what-is-zimbra',
        'zimbra/ansible-zimbra-single',
        ],
    },
    {
      type: 'category',
      label: 'Ubuntu Recipes',
      items: [
        'ubuntu/ubuntu-20-04-on-hyper-v',
        'ubuntu/ubuntu-lxd-reference',
        'ubuntu/comply-with-cis-or-disa-stig-on-ubuntu',
        'ubuntu/customizing-the-cis-profile',
        'ubuntu/docker-rootless-mode',
        ],
    },
    {
      type: 'category',
      label: 'ADempiere Recipes',
      items: [
        'adempiere/adempiere',
        'adempiere/install-on-ubuntu',
        ],
    },
    {
      type: 'category',
      label: 'Docusaurus Recipes',
      items: [
        'getting-started',
        'create-a-page',
        'create-a-document',
        'create-a-blog-post',
        'markdown-features',
        'thank-you',
      ],
    },
  ],
};

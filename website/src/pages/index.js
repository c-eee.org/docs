import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Enterprise Recipes',
    imageUrl: 'img/enterprise_recipes.svg',
    description: (
      <>
       All C-EEE recipes are enterprise-ready opensource software deployment recipes designed 
       to fulfill the needs of modern organizations. They are all based on real-world business 
       scenarios.To the best of our abilities, knowledge, and expertise, we have hardened 
       and secured them. However, we cannot guarantee that it will work in your specific
       setting for a variety of reasons. You may need to change them to match your needs. 
      </>
    ),
  },
  {
    title: 'Enterprise Academy',
    imageUrl: 'img/enterprise_academy.svg',
    description: (
      <>
        C-EEE Enterprise Academy is not yet another vendor-biased certification  training
        & exam conducting institution.  Instead, we assist you in developing skills relevant   
        toyour company open source project. Having in-house expertise is the best way to 
        sustain your open source enterprise deployment and keep your software running for a 
        longer period of time. 
      </>
    ),
  },
  {
    title: 'Enterprise Consulting',
    imageUrl: 'img/enterprise_consulting.svg',
    description: (
      <>
        C-EEE Enterprise Recipes and Blogs may not always meet all of your deployment
        requirements. Don't be worried; we're here to help. You can hire an expert freelance 
        consultant via our Discord channel to assist with efficiently deploying your project 
        in your environment. You can reach us by email at <b>enterprise.support@c-eee.org</b>.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Learn More
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

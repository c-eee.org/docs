/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'C-EEE | Community Entrusted Enterprise Edition',  

  tagline: 'We use, promote, develop, and support the People Entrusted            \
            Libre Software domain which fulfills your dream              \
            for an on-premise private digital libre workspace.           \
            It enables you to own the technology you use to obtain       \
            more strategic influence and stay ahead of your competitors. \
            This website is dedicated to those who are interested in     \
            the concepts of digital sovereignty, data sovereignty,       \
            and sovereignty clouds,   Use our enterprise recipes, deploy \
            it yourself, and have it validated by C-EEE.ORG and you be ready  \
            to march toward your ISO/IEC 27001 certification.',                                  
  url: 'https://c-eee.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'c-eee.org', // Usually your GitHub org/user name.
  projectName: 'docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Home',
      logo: {
        alt: 'C-EEE.ORG Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/c-eee.org/docs',
          label: 'Gitlab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Get Fulfill',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'Gitlab',
              href: 'https://gitlab.com/c-eee.org/docs',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} C-EEE.ORG. Built with Docusaurus and Gitlab CI/CD`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/c-eee.org/docs/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/c-eee.org/docs/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};

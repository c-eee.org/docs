---
slug: Continuous ERP modernization with CI/CD
title: Continuous ERP modernization with CI/CD
author: Chithaka Deshapriya
author_title: C-EEE Founder
author_url: https://github.com/cdeshapriya
tags: 
  - c-eee
  - opensource
  - erp
  - continuous
  - ci/cd 
---
## Reference

[Continuous ERP modernization with CI/CD](https://metasfresh.com/en/2021/10/13/continuous-erp-modernization-with-ci-cd/)

---
title: Learn More
slug: /
---

## Introduction to C-EEE 

**C-EEE** was founded to promote the idea of providing 100% free and open source software, which we call **"Community Entrusted Enterprise Edition"** and which is empowered by its user community.  The user community collaborates to deploy and maintain opensource software that fulfills the needs of modern commercial organizations.  For example, the product may lack SSO with MFA authentication capabilities and DR Methodology by default. In this case, a community member or organization steps up to fill the gap, making your product enterprise-ready. . One of C-EEE's primary goals is to provide you with a comprehensive Open Knowledge Base that includes enterprise-class Deployment Recipes, a Blog System, training, and support based on an ecosystem of carefully selected open source software with the goal of creating a Libre Digital Private Workplace for your company. Making a thorough enterprise deployment recipes or tutorials repository available for a certain free and opensource software makes it enterprise ready, which is one of the creator of C-EEE's primary goals.

## Your Data is Yours

### Libre Workplace

Libre Workplace is geared to people whose primary business goal is data privacy and security, and who want to immerse themselves in the concept of an On-Site Private, Digital Workplace. 

### Using Opensource to Gain the Power

If you want to create your own Private Digital Workplace, you must own the technology you use. You have more authority when you own the technology you use than when you license someone else's technology. You should be able to examine and audit what exactly your software code does.

### Free Food Recipe vs. Free Food Serving

Many people might fail to understand the difference between freeware and open source software.   The only way to own the software technology you use is to have a complete copy of the source code or programming code, which is also comes free.  Consider the distinction between a free recipe and a free serving of food.  You can't do anything with a free serving of food other than eat it if it tastes good, but you have no idea what it includes, how healthy it is, and so on.
 
On the other hand, free food recipes provide you more power because they allow you to add more components to it, prepare it the way you want, know what it contains, and how healthy it is. You may now compare this to a free copy of pre-built binary of antivirus software vs free and opensource antivirus software that comes with a complete copy of its source code. It is quite natural that you will recognize what is more powerful. 

## If You Fail, So Do We

We don't want your Enterprise Open Source Deployment Project to fail because you lacked specific skills in-house or with your preferred service provider. If you can't find the answers to your queries in our existing knowledge base, please contact us. If you require a new deployment recipe for your project, please do not hesitate to contact us. If you enable us to publish the prepared recipe in the C-EEE document base, there will be no payment.' On the other side, we believe that allowing you to fail is a disrespect to the tremendous development community that provides free and opensource software to the community.

## C-EEE Candidate Open-Source Software

We chose ADempiere|IDempiere ERP, Percona PostgreSQL DB, Ubuntu|Debian Linux OS, Keycloak IAM, WSO2 IAM, Zimbra Community Email and Collaboration, Proxmox Virtualization, TrueNAS Scale Storage, OPNsense Firewall, Jasper Reports Server for BI, NGINX, Samba AD/DC, GitLab DevSecOps, ZabbiX Monitoring and many other opensource software technologies for integration. Let us collaborate to ensure its success. We chose these softwares because they are either C-EEEs by default or can be improved to be C-EEEs.



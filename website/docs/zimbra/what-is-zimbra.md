---
title: What is Zimbra
---

## Overview

Zimbra Collaboration - Open Source Edition is an email, calendar, and collaboration server that is open source. Zimbra, which is trusted by over 500 million open source users globally, increases user productivity on any desktop and drastically decreases TCO when compared to legacy platform suppliers.

## Features

 

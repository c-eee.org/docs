---
title: ADempiere References
---

## ADempiere

The world's most powerful community-developed ERP system. Since its inception, it has been 100% open-source and license-free, multi-tenant, and multi-currency.

## References

- [ADempiere Documentation](https://adempiere.gitbook.io/docs/)

- [Development Environments](https://adempiere.gitbook.io/docs/developer-guide/development-environments)

- [3 Easy Ways To Install Eclipse on Ubuntu](https://www.linuxfordevices.com/tutorials/ubuntu/install-eclipse-on-ubuntu)

- [Create your ADempiere customization environment](https://www.adempierebr.com/Create_your_ADempiere_customization_environment)

- [How to setup a customization environment for ADempiere software](https://adempiere.gitbook.io/docs/developer-guide/development-environments/customization-environment)

- [ADempiere Technical Training](https://en.wikiversity.org/wiki/ADempiere_Technical_Training)

- [Creating a New Seed Database](https://www.adempierebr.com/Creating_a_New_Seed_Database)

- [Installing ADempiere 3.7.0 on Ubuntu 11.04](https://www.adempierebr.com/Installing_ADempiere_3.7.0_on_Ubuntu_11.04)

- [iDempiere Installing from Installers](https://wiki.idempiere.org/en/Installing_from_Installers)

- [How to create Ant build file for existing Java project in Eclipse](https://www.codejava.net/ides/eclipse/how-to-create-ant-build-file-for-existing-java-project-in-eclipse)

- [Financial Reporting in Adempiere](https://wtcindia.wordpress.com/2011/12/07/financial-reporting-in-adempiere/)

- [Adempiere Report](https://www.adempierebr.com/Report)

- [Adempiere Report](https://www.adempierebr.com/Reporting)

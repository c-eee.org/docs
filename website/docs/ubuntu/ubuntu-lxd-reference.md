---
title: Ubuntu LXD References
---

## References

- [What is virtualization? A beginner’s guide](https://ubuntu.com/blog/virtualization)

- [Containerization vs. Virtualization : understand the differences](https://canonical.com/blog/containerization-vs-virtualization)

- [Linux Containers (LXC)](https://www.cyberciti.biz/faq/category/linux-containers-lxc/)

- [How To Install LXD on Ubuntu 20.04 LTS using apt](https://www.cyberciti.biz/faq/install-lxd-on-ubuntu-20-04-lts-using-apt/)

- [How to configure ufw to forward port 80/443 to internal server hosted on LAN](https://www.cyberciti.biz/faq/how-to-configure-ufw-to-forward-port-80443-to-internal-server-hosted-on-lan/)

- [How To Set Up a Firewall with UFW on Debian 12](https://www.cyberciti.biz/faq/set-up-a-firewall-with-ufw-on-debian-12-linux/)

- [Ubuntu 22.04 Set Up UFW Firewall in 5 Minutes](https://www.cyberciti.biz/faq/ubuntu-22-04-lts-set-up-ufw-firewall-in-5-minutes/)

- [How to rename LXD / LXC container](https://www.cyberciti.biz/faq/how-to-rename-lxd-lxc-linux-container/)

- [How to backup and restore LXD containers](https://www.cyberciti.biz/faq/how-to-backup-and-restore-lxd-containers/)

- [How to run Docker inside LXD containers](https://ubuntu.com/tutorials/how-to-run-docker-inside-lxd-containers)

---
title: Run the Docker daemon as a non-root user
---

## Reference

- [Run the Docker daemon as a non-root user](https://docs.docker.com/engine/security/rootless/)

- [Installing and securing Docker rootless for production use](https://medium.com/@flavienb/installing-and-securing-docker-rootless-for-production-use-8e358d1c0956)

- [How to do a Rootless Docker Installation (on Ubuntu and Debian)](https://linuxhandbook.com/rootless-docker/)
